# -*- coding: utf-8 -*-
"""
Created on 9/27/2021, 8:50:16 PM

@author:
Akash Raman a.raman@utwente.nl
PhD candidate, Mesoscale Chemical Systems, University of Twente

This script acquires images from a pco camera and displays it live on an
OpenCV window. The script continuously prints the actual FPS of recording.
No images are saved, although it is implemented in a comment.

This script initializes (if required), starts and records open circuit voltage
data from a Biologic potentiostat and displays it live on an GUI canvas.

All this is done on a simple GUI

"""

from datetime import datetime
import pandas as pd
import os
import PySimpleGUI as GUI
import cv2
import pco
import time
from dataclasses import dataclass
from kbio.kbio_api import KBIO_api
from kbio.c_utils import c_is_64b
from kbio.utils import exception_brief
import kbio.kbio_types as KBIO
import sys
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, FigureCanvasAgg
from matplotlib.figure import Figure
import numpy as np
from kbio.kbio_tech import ECC_parm, make_ecc_parm, make_ecc_parms
import threading
from kbio.tech_types import TECH_ID


class myGUI:
    def __init__(self):
        # Defining GUI Layout
        GUI.theme('Reddit')

        # This defines the part of the GUI with camera exposure controls
        exposure_layout = [
            [GUI.InputText('', key=('input_exposure'),
                           size=(button_width, 1))],
            [GUI.Button('Set exposure', key='button_exposure',
                        size=(button_width, 1))]
        ]

        # This defines the part of the GUI with potentiostat controls
        pstat_layout = [
            [GUI.Frame('Current (A)', self.create_inputFrame(
                "input_current", '10e-6'), size=(button_width, 1))],
            [
                GUI.Frame('Current range', [
                    [GUI.Combo(['I_RANGE_1uA', 'I_RANGE_10uA', 'I_RANGE_100uA',
                                'I_RANGE_1mA', 'I_RANGE_10mA', 'I_RANGE_100mA',
                                'I_RANGE_1A'], key='list_cRange',
                               default_value='I_RANGE_10uA')]
                ], size=(button_width, 1))
            ],
            [GUI.Frame('Record dt (s)', self.create_inputFrame(
                "input_dt", '0.1'), size=(button_width, 1))],
            [GUI.Button('Initialize p-stat', key='button_set',
                        size=(button_width, 1))]
        ]

        # This defines the first column (left) of the GUI
        buttons_column = [
            [GUI.Frame('Path and filename for saving experiment', [
                [GUI.InputText('D:/Work/Experimental/Experimental_support_protocols/cameraPotentiostatGUI', key='input_path',
                               size=(button_width * 2, 3))],
                [GUI.InputText('', key='input_filename',
                               size=(button_width, 1))]
            ], size=(button_width, 3))],
            [GUI.FolderBrowse(target='input_path',
                              initial_folder="D:/Work/Experimental/Experimental_support_protocols/cameraPotentiostatGUI")],
            [GUI.Button('Start', key='button_rec', size=(button_width, 1))],
            [GUI.Button('Stop', key='button_stop', size=(button_width, 1))],
            [GUI.Frame('Interframe time [ms]', self.create_SpinFrame
                       (
                           "spin_frametime", 100, [
                               10 * i for i in range(1, 21)]
                       ))
             ],
            [GUI.Frame('Exposure [s]', exposure_layout)],
            [GUI.Frame('P-stat controls', pstat_layout)]
        ]

        # This defines the middle column of the GUI with live image
        # and FPS display
        image_column = [
            [GUI.Text('Live feed', size=(60, 1), justification='center')],
            [GUI.Image(filename='', key='videoFeed')],
            [
                GUI.Frame('FPS', self.create_TextFrame("text_FPS"),
                          size=(button_width, 1)),
                GUI.Frame('Average FPS', self.create_TextFrame('text_Av_FPS'),
                          size=(button_width, 1))
            ]
        ]

        # Defining rigth most, plotting column
        canvas_size = (300, 300)
        plot_column = [
            [GUI.Canvas(size=canvas_size, key='canvas_potentiostat')]]

        # This completes the top level GUI design with vertical separators
        window_layout = [
            [GUI.Text(
                '\nसर्वस्य लोचनं शास्त्रं, यस्य नास्त्यन्ध एव सः ॥', font=("Arial", 15),
                tooltip=("Science is the eye of everyone, one who hasn’t got"
                         " it, is like a blind (Hitopadesha 0.10)"))],
            [
                GUI.Column(buttons_column, element_justification='center'),
                GUI.VSeparator(),
                GUI.Column(image_column, element_justification='center'),
                GUI.VSeparator(),
                GUI.Column(plot_column, element_justification='center')
            ]

        ]

        # This creates the window and makes it resizable
        self.window = GUI.Window(
            'MCEC_Bubbles_project: PCO + Biologic experiment',
            window_layout, resizable=True, finalize=True,
            location=(50, 50), element_justification='c')

        # Create and initialize plot, canvas, figure aggregate
        self.canvas = self.window['canvas_potentiostat'].TKCanvas
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel("Time [s]")
        self.ax.set_ylabel("Potential [V]")
        self.ax.grid()
        self.fig_agg = self.draw_figure(self.canvas, self.fig)

    def create_TextFrame(self, key_string):
        """
        This function creates a pySimpleGUI frame with a text element inside.

        Parameters
        ----------
        key_string : string
            This is the key that refers to the text element returned.

        Returns
        -------
        layout : list
            List defining a frame with text element with specified key.

        """
        layout = [
            [GUI.Text('', key=key_string, size=(button_width, 1))]
        ]
        return layout

    def create_inputFrame(self, key_string, _default_text):
        """
        This function creates a pySimpleGUI frame with a text input element
        inside.

        Parameters
        ----------
        key_string : string
            This is the key that refers to the text element returned.

        _default_text : string
            This is the default text that appears in the input box

        Returns
        -------
        layout : list
            List defining a frame with text element with specified key.

        """
        layout = [
            [GUI.InputText(_default_text, key=key_string, size=(
                button_width, 1))]
        ]
        return layout

    def create_SpinFrame(self, key_string, init_val, vals):
        """
        This function creates a pySimpleGUI frame with a spin element
        inside with specified values.

        Parameters
        ----------
        key_string : string
            This is the key that refers to the spin element returned.
        init_val : int
            Initial value of the spin element.
        vals : list
            List of allowed values of the spin element.

        Returns
        -------
        layout : list
            List defining a frame with spin element with specified key and
            parameters.

        """
        layout = [
            [GUI.Spin(vals, key=key_string,
                      initial_value=init_val, size=(button_width, 1))
             ]
        ]
        return layout

    def draw_figure(self, canvas, figure, loc=(0, 0)):
        """
        This function creates a canvas + figure aggregate and returns it.

        Parameters
        ----------
        canvas : Canvas object tkinter module
        figure : matplotlib Figure object
        loc : tuple, optional
            Location of figure. The default is (0, 0).

        Returns
        -------
        figure_canvas_agg : FigureCanvasTkAgg object

        """
        figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
        figure_canvas_agg.draw()
        figure_canvas_agg.get_tk_widget().pack(side='top', fill='both',
                                               expand=1)
        return figure_canvas_agg


class Potentiostat:
    def __init__(self, current_set, current_range_set, dt_set):
        # Potentiostat parameters to be adjusted
        verbosity = 2
        address = "USB0"
        self.channel = 1

        binary_path = "./ECLab_dlls_apis/"

        # CP parameter values
        cp3_tech_file = "cp.ecc"
        cp4_tech_file = "cp4.ecc"

        repeat_count = 0
        self.record_dt = dt_set  # seconds
        i_range = current_range_set

        # dictionary of CP parameters (non exhaustive)
        CP_parms = {
            'current_step':  ECC_parm("Current_step", float),
            'step_duration': ECC_parm("Duration_step", float),
            'vs_init':       ECC_parm("vs_initial", bool),
            'nb_steps':      ECC_parm("Step_number", int),
            'record_dt':     ECC_parm("Record_every_dT", float),
            'record_dE':     ECC_parm("Record_every_dE", float),
            'repeat':        ECC_parm("N_Cycles", int),
            'I_range':       ECC_parm("I_Range", int),
        }

        # defining a current step parameter
        @ dataclass
        class current_step:
            current: float
            duration: float
            vs_init: bool = False

        # list of step parameters
        steps = [
            current_step(current_set, 10000000),  # 10uA during 10000000s
        ]

        # Initialize potentiostat, check if firmware is loaded, load if
        # necessary. Define experimental parameters and load technique.
        # Then start channel
        try:
            DLL_path = self.find_DLL_type(binary_path)

            self.api, self.id_, self.tech_file = self.setupDevice(
                DLL_path, address, self.channel, cp3_tech_file, cp4_tech_file)

            p_steps = list()

            for idx, step in enumerate(steps):
                parm = make_ecc_parm(
                    self.api, CP_parms['current_step'], step.current, idx)
                p_steps.append(parm)
                parm = make_ecc_parm(
                    self.api, CP_parms['step_duration'], step.duration, idx)
                p_steps.append(parm)
                parm = make_ecc_parm(
                    self.api, CP_parms['vs_init'], step.vs_init, idx)
                p_steps.append(parm)

            # number of steps is one less than len(steps)
            p_nb_steps = make_ecc_parm(self.api, CP_parms['nb_steps'], idx - 1)

            # record parameters
            p_record_dt = make_ecc_parm(
                self.api, CP_parms['record_dt'], self.record_dt)

            # repeating factor
            p_repeat = make_ecc_parm(
                self.api, CP_parms['repeat'], repeat_count)
            p_I_range = make_ecc_parm(
                self.api, CP_parms['I_range'], KBIO.I_RANGE[i_range].value)

            # make the technique parameter array
            ecc_parms = make_ecc_parms(
                self.api, *p_steps, p_nb_steps, p_record_dt,  # p_record_dE,
                p_I_range, p_repeat)

            # BL_LoadTechnique
            self.api.LoadTechnique(self.id_, self.channel, self.tech_file,
                                   ecc_parms, first=True, last=True,
                                   display=(verbosity > 1))

        except KeyboardInterrupt:
            print(".. interrupted")

        except Exception as e:
            self.print_exception(e, verbosity)

    def print_exception(self, e, verbosity):
        print(f"{exception_brief(e, verbosity>=2)}")

    def print_messages(self, ch, api, id_):
        """Repeatedly retrieve and print messages for a given channel."""
        while True:
            msg = api.GetMessage(id_, ch)
            if not msg:
                break
            print("> new messages :")
            print(msg)

    def find_DLL_type(self, binary_path):
        # determine library file according to Python version (32b/64b) and
        # return path for DLL file
        if c_is_64b:
            print("> 64b application")
            DLL_file = "EClib64.dll"
        else:
            print("> 32b application")
            DLL_file = "EClib.dll"

        return binary_path + DLL_file

    def setupDevice(self, DLL_path, address, channel, ocv3_tech_file,
                    ocv4_tech_file):
        # open the DLL,
        # connect to the device using its address,
        # retrieve the device channel info,
        # test whether the proper firmware is running,
        # if it is, print all the messages this channel has accumulated so far

        # API initialize
        api = KBIO_api(DLL_path)

        # BL_GetLibVersion
        version = api.GetLibVersion()
        print(f"> EcLib version: {version}")

        # BL_Connect
        id_, device_info = api.Connect(address)
        print(f"> device[{address}] info :")
        print(device_info)

        # detect instrument family
        is_VMP3 = device_info.model in KBIO.VMP3_FAMILY
        is_VMP300 = device_info.model in KBIO.VMP300_FAMILY

        # BL_TestConnection
        ok = "OK" if api.TestConnection(id_) else "not OK"
        print(f"> device[{address}] connection : {ok}")

        # BL_GetChannelsPlugged
        # .. PluggedChannels is a generator, expand into a set
        channels = {*api.PluggedChannels(id_)}
        print(f"> device[{address}] channels : {channels}")

        # test whether the configured channel exists
        if channel not in channels:
            print(
                f"Configured channel {channel} does not belong to device\
                 channels {channels}")
            sys.exit(-1)

        # BL_GetChannelInfos
        channel_info = api.GetChannelInfo(id_, channel)
        print(f"> Channel {channel} info :")
        print(channel_info)

        if not channel_info.is_kernel_loaded:
            if is_VMP3:
                firmware_path = "kernel.bin"
                fpga_path = "Vmp_ii_0437_a6.xlx"
            elif is_VMP300:
                firmware_path = "kernel4.bin"
                fpga_path = "vmp_iv_0395_aa.xlx"
            else:
                firmware_path = None

            if firmware_path:
                print(f"> Loading {firmware_path} ...")
                # create a map from channel set
                channel_map = api.channel_map({channel})
                # BL_LoadFirmware
                # , force=load_firmware)
                api.LoadFirmware(id_, channel_map,
                                 firmware=firmware_path, fpga=fpga_path)
                print("> ... firmware loaded")

        # pick the correct ecc file based on the instrument family
        tech_file = ocv3_tech_file if is_VMP3 else ocv4_tech_file

        # BL_GetMessage
        print("> messages so far :")
        self.print_messages(channel, api, id_)

        return api, id_, tech_file

    def decode_experiment_data(self, data):
        """Unpack the experiment data, decode it according to the technique,
           display it, then return the experiment status """

        current_values, data_info, data_record = data

        t_data = np.array([])
        Ewe_data = np.array([])
        current_data = np.array([])
        cycle_data = np.array([])

        status = current_values.State
        status = KBIO.PROG_STATE(status).name

        tech_name = TECH_ID(data_info.TechniqueID).name

        ix = 0

        for _ in range(data_info.NbRows):

            inx = ix + data_info.NbCols
            t_high, t_low, *row = data_record[ix:inx]

            nb_words = len(row)
            if nb_words != 3:
                raise RuntimeError(
                    f"{tech_name} : unexpected record length ({nb_words})")

            # Ewe is a float
            Ewe = self.api.ConvertNumericIntoSingle(row[0])

            # current is a float
            current = self.api.ConvertNumericIntoSingle(row[1])

            # technique cycle is an integer
            cycle = row[2]

            # compute timestamp in seconds
            t_rel = (t_high << 32) + t_low
            t = current_values.TimeBase * t_rel

            t_data = np.append(t_data, t)
            Ewe_data = np.append(Ewe_data, Ewe)
            current_data = np.append(current_data, current)
            cycle_data = np.append(cycle_data, cycle)

            ix = inx

        parsed_row = {'t': t_data, 'Ewe': Ewe_data,
                      'I': current_data, 'cycle': cycle_data}
        return parsed_row, status


class Camera:
    def __init__(self):
        self.cam = pco.Camera()
        self.cam.configuration = {'binning': (4, 4),
                                  'roi': (1, 1, 1280, 1280), }
        # Set exposure time based on binning.
        # !!! Feature to select binning is not implemented and is hard coded.
        if self.cam.configuration['binning'] == (2, 2):
            self.cam.set_exposure_time(6e-5)
        elif self.cam.configuration['binning'] == (1, 1):
            self.cam.set_exposure_time(1e-3)
        elif self.cam.configuration['binning'] == (4, 4):
            self.cam.set_exposure_time(29928e-9)
        else:
            self.cam.set_exposure_time(input('Enter exposure time [s]'))

        self.cam.record(number_of_images=100, mode='ring buffer')
        self.cam.wait_for_first_image()

    def getImage(self):
        # Read latest image in ring buffer
        image, meta = self.cam.image(0xFFFFFFFF)

        # Normalize image from camera
        normalizedImage = cv2.normalize(image, dst=None, alpha=0, beta=65535,
                                        norm_type=cv2.NORM_MINMAX)

        # Convert normalized image to 8 bit image
        normalizedImage = (normalizedImage/256).astype('uint8')

        # reducing image size for display
        resizedImage = cv2.resize(normalizedImage, (0, 0), fx=0.4, fy=0.4)

        # convert image to png format
        encodedImage = cv2.imencode('.png', resizedImage)[1].tobytes()

        return normalizedImage, encodedImage

    def closeConnection(self):
        self.cam.stop()
        self.cam.close()


def pstatLoop():
    global live_data

    while True:
        if stop_flag or pstat_init_flag:
            break
        time.sleep(0.05)

    if not stop_flag:
        print(">>Initializing pstat")
        # Initialize potentiostat
        potentiostat_obj = Potentiostat(current_set, current_range_set, dt_set)

    while True:
        if stop_flag or exp_running_flag:
            break
        time.sleep(0.15)

    if not stop_flag:
        print(">>Starting pstat channel")
        potentiostat_obj.api.StartChannel(
            potentiostat_obj.id_, potentiostat_obj.channel)

    while not stop_flag:
        # print(">>pstatLoop acquiring")
        # BL_GetData
        data = potentiostat_obj.api.GetData(
            potentiostat_obj.id_, potentiostat_obj.channel)

        pData, status = potentiostat_obj.decode_experiment_data(data)
        pData.pop('cycle')
        temp_df = pd.DataFrame(pData)

        live_data = live_data.append(temp_df)
        live_data.to_csv(filename, index=False)

        time.sleep(0.25)

        # Stop recording and potentiostat data acquisition if Stop button is pressed.
        if status == 'STOP':
            stop_flag.value = 0
            break

    print(">>pstatLoop stopping")
    if 'potentiostat_obj' in locals():
        # BL_StopChannel
        potentiostat_obj.api.StopChannel(
            potentiostat_obj.id_, potentiostat_obj.channel)

        # BL_Disconnect
        potentiostat_obj.api.Disconnect(potentiostat_obj.id_)
        print(">>pstat disconnected")


def camLoop():
    global exposure_value, exposure_set_flag
    global frame_rate, av_frame_rate, live_image

    camera_obj = Camera()
    exposure_value = camera_obj.cam.configuration['exposure time']

    prev_time = 0       # Time at end of prev loop
    current_time = 0    # Time at end of current loop
    num_frames = 0      # Number of frames acquired from camera (for
    # average FPS calculation)
    start_time = datetime.now().timestamp()  # Acquision start time

    while not stop_flag:
        TimeStamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")
        normalizedImage, encodedImage = camera_obj.getImage()

        if exp_running_flag:
            cv2.imwrite('img_' + TimeStamp + str('.png'), normalizedImage)

        if exposure_set_flag:
            print(">Changing camera exposure")
            exposure_set_flag = False
            camera_obj.cam.set_exposure_time(exposure_value)

        # Send image to global variable for GUI
        live_image = encodedImage

        # Calculate frame rates
        current_time = datetime.now().timestamp()
        frame_rate = 1 / (current_time - prev_time)
        av_frame_rate = num_frames / (current_time - start_time)

        prev_time = current_time
        num_frames += 1

        time.sleep(inter_frame_time)

    print(">Camera closing")
    camera_obj.closeConnection()
    print(">Camera closed")


def GUILoop():
    global inter_frame_time, exposure_value, filename
    global stop_flag, exp_running_flag,  exposure_set_flag, pstat_init_flag
    global current_set, current_range_set, dt_set

    line, = GUI_obj.ax.plot([], [])

    # Update initial exposure value in GUI
    time.sleep(1)
    GUI_obj.window['input_exposure'].update(str(exposure_value))

    print("[+]Entering GUI Loop")

    # Run until Ctrl + C input, Stop button is pressed or GUI closed
    while(True):
        try:
            event, values = GUI_obj.window.read(timeout=1)  # Timeout is in ms
            if event == 'Exit' or event == GUI.WIN_CLOSED or \
                    event == 'button_stop':
                break

            # If new exposure set button is pressed, read and set exposure
            elif event == 'button_exposure':
                if float(values['input_exposure']) > 0:
                    exposure_set_flag = True
                    exposure_value = float(values['input_exposure'])
                    # Update exposure shown in GUI to match camera exposure
                    GUI_obj.window['input_exposure'].update(
                        str(exposure_value))

            GUI_obj.window['videoFeed'].Update(data=live_image)

            if exp_running_flag:
                # If potentiostat is recording, acquire data and update plot
                line.set_xdata(live_data['t'])
                line.set_ydata(live_data['Ewe'])

                GUI_obj.ax.relim()
                GUI_obj.ax.autoscale_view()
                GUI_obj.fig_agg.draw()

            elif not pstat_init_flag:
                # If "Set" button is pressed and if all fields are filled,
                # initialize potentiostat with experimental params from
                if (
                    event == 'button_set' and bool(values['input_current'])
                    and bool(values['list_cRange']) and bool(values['input_dt'])
                    and values['input_path'] and values['input_filename']
                ):
                    current_set = float(values['input_current'])
                    current_range_set = values['list_cRange']
                    dt_set = float(values['input_dt'])
                    filename = values['input_filename']
                    pstat_init_flag = True
                    time.sleep(1)
                    if os.path.isdir(values['input_path']):
                        os.chdir(values['input_path'])
                    else:
                        os.mkdir(values['input_path'])
                        os.chdir(values['input_path'])

                    print("+Initialization values set")

            elif pstat_init_flag:
                # If record button is pressed, start experiment, trigger
                # potentiostat and camera save
                if event == 'button_rec':
                    exp_running_flag = True

            # Update frame rates in GUI
            GUI_obj.window['text_FPS'].Update(str(frame_rate)[:6])
            GUI_obj.window['text_Av_FPS'].Update(str(av_frame_rate)[:6])

            if float(values['spin_frametime']) > 0:
                inter_frame_time = float(values['spin_frametime']) / 1000

        # Escape sequence using Ctrl + C key combination
        except KeyboardInterrupt:
            break

    exp_running_flag = False
    stop_flag = True

    print("> experiment done")


if __name__ == '__main__':
    button_width = 12

    stop_flag = False
    exp_running_flag = False
    exposure_set_flag = False
    pstat_init_flag = False

    inter_frame_time = 0.2
    exposure_value = 0
    frame_rate = 0
    av_frame_rate = 0
    live_image = bytes()

    image = []

    current_set = 0
    current_range_set = ""
    dt_set = 0
    filename = ""

    live_data = pd.DataFrame([], columns=['t', 'Ewe', 'I'])

    GUI_obj = myGUI()

    camLoop_ps = threading.Thread(target=camLoop, args=())
    camLoop_ps.start()

    pstatLoop_ps = threading.Thread(target=pstatLoop, args=())
    pstatLoop_ps.start()

    time.sleep(2)

    GUILoop()

    camLoop_ps.join()
    pstatLoop_ps.join()
    GUI_obj.window.Close()
